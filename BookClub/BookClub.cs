using System.Collections.Generic;
using System;
using System.Linq;
using System.Xml.Linq;

namespace BookClub
{
    public class BookClub : IBookClub
    {
        public List<IBook> Books { get; internal set; }
        public String FilePath { get;}

        public BookClub(string filepath)
        {
            this.FilePath = filepath;
        }

        public void LoadData()
        {
            var bookXDoc = XDocument.Load($"{this.FilePath}/books.xml").Root;
            var ratingXDoc = XDocument.Load($"{this.FilePath}/ratings.xml").Root;
            var allBooksXml = bookXDoc.Elements("book");
            var ratingsXml = ratingXDoc.Elements("book");
            // Create Anonymous objects for each book
            var books = allBooksXml.Select(b => new
            {
                id = b.Attribute("id").Value,
                title = b.Element("title").Value,
                desc = b.Element("description").Value,
                genre = b.Element("genre").Value,
                authLName = b.Element("author").Attribute("lastName").Value,
                authFName = b.Element("author").Attribute("firstName").Value
            });
            // Create Anonymous objects for each rating
            var ratings = from rating in ratingsXml
                          select new
                          {
                              id = rating.Attribute("id").Value,
                              rating = CalculateAverageRating(rating),
                              numberOfReaders = rating.Elements("rating").Count()
                          };
            // Join books and ratings
            var allBooks = from b in books
                           join r in ratings on b.id equals r.id
                           select new
                           {
                               id = b.id,
                               title = b.title,
                               desc = b.desc,
                               genre = b.genre,
                               authLName = b.authLName,
                               authFName = b.authFName,
                               rating = r.rating,
                               numberOfReaders = r.numberOfReaders
                           };
            // Create Book objects
            this.Books = allBooks.Select(b => new 
            Book(int.Parse(b.id), b.title, b.desc, b.genre, b.authLName, b.authFName, b.rating, b.numberOfReaders) as IBook).ToList();
        }

        /// <summary>
        /// Calculate the average rating for a book
        /// </summary>
        /// <param name="bookRating">XElement of the bookRatings</param>
        private double CalculateAverageRating(XElement bookRating)
        {
            return bookRating.Elements("rating").Select(r => int.Parse(r.Value)).Average();
        }
    }
}