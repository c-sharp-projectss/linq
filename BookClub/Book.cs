namespace BookClub
{
    public class Book : IBook
    {

        public int BookId { get; }

        public string Title { get; }

        public string Description { get; }

        public string Genre { get; }

        public string AuthorLastName { get; }
        public string AuthorFirstName { get; }

        public double Rating { get; }

        public int NumberOfReaders { get; }
        public Book(int id, string title, string description, string genre, 
            string authorLastName, string authorFirstName, double rating, int numberOfReaders)
        {
            BookId = id;
            Title = title;
            Description = description;
            Genre = genre;
            AuthorLastName = authorLastName;
            AuthorFirstName = authorFirstName;
            Rating = rating;
            NumberOfReaders = numberOfReaders;
        }

        public override string ToString()
        {
            // return $"BookId: {BookId}, Title: {Title}, Description: {Description}, Genre: {Genre}, Author: {AuthorLastName}-{AuthorFirstName}, Rating: {Rating}, NumberOfReaders: {NumberOfReaders}";
            return $"BookId: {BookId}, Title: {Title}, Genre: {Genre}, Author: {AuthorLastName}-{AuthorFirstName}, Rating: {Rating}, NumberOfReaders: {NumberOfReaders}";
        
        }
    }
}