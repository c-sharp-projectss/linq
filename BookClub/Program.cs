﻿using System;
using System.Linq;
using System.Xml.Linq;

namespace BookClub
{
    class Program
    {
        static void Main(string[] args)
        {
            BookClub bc = new BookClub("./xml");
            bc.LoadData();

            Console.WriteLine("Welcome to the Book Club!");
            Console.WriteLine($"Here are The books in our club:");
            // Print the books
            bc.Books.ForEach((b) => Console.WriteLine(b));
            // menu
            while (true)
            {
                Console.WriteLine("\n\nEnter 1 to View the top-rated books.");
                Console.WriteLine("Enter 2 to Browse books by genre.");
                Console.WriteLine("Enter 3 to search for a book by keyword.");
                Console.WriteLine("Enter 4 for a SURPRISE.");
                Console.Write("Enter Option number(type q to quit):");
                string userInput = Console.ReadLine();
                switch (userInput)
                {
                    case "1":
                        Console.WriteLine("Here are the top 5 rated books:");
                        bc.Books.OrderByDescending(b => b.Rating).Take(5).ToList().ForEach(b => Console.WriteLine(b));
                        break;
                    case "2":
                        // Write a LINQ query to get all the books grouped by genre, sorted first by the sum of NumberOfReaders in descending order and then by average rating in descending order.
                        var result = bc.Books.GroupBy(b => b.Genre).OrderByDescending(g => g.Sum(b => b.NumberOfReaders)).
                                    OrderByDescending(g => g.Sum(b => b.Rating));
                        result.ToList().ForEach(g =>
                        {
                            Console.WriteLine($"Genre: {g.Key.ToString()}");
                            g.ToList().ForEach(b => Console.WriteLine($"Book title: {b.Title}"));
                            Console.WriteLine($"Number Of Books: {g.Count()}");
                            var averageRating = g.Select(b => b.Rating).Average();
                            Console.WriteLine($"Average Rating of Genre: {averageRating}");
                        });
                        break;
                    case "3":
                        Console.WriteLine("Enter the keyword for the book: ");
                        string keyword = Console.ReadLine();
                        var booksWithKeyword = (
                            from b in bc.Books
                            where b.Title.Contains(keyword)
                            select b).Union((
                              from b in bc.Books
                              where b.Description.Contains(keyword)
                              select b
                            )).Distinct();
                        // Check if any books have the keyword
                        if (booksWithKeyword.Count() == 0)
                        {
                            Console.WriteLine("No books found!");
                        }
                        else
                        {
                            booksWithKeyword.ToList().ForEach((b) => Console.WriteLine(b));
                        }
                        break;
                    case "4":
                        Console.WriteLine("As I love harry potter it was only right to display the only Harry Potter reference!");
                        var harryPotterBook = from b in bc.Books where b.Genre.Contains("Harry Potter") select b;
                        harryPotterBook.ToList().ForEach(b => Console.WriteLine(b));
                        break;
                    case "q":
                        Console.WriteLine("Goodbye!");
                        return;
                }

            }
        }

    }
}
